﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PDBParser : MonoBehaviour {

    public string file = "";
    public float scale;
    public GameObject prefab;
    public Transform atomParent;
    public Transform dataParent;
    //public GameObject surface;
    public Material[] mats;
    public enum Groups { CONS, ID, Fifth };
    public Groups groupBy;
    public GameObject meshPrefab;
    public int chunkSize;
    public int sets;
    
    private TextAsset f;
    private string[] lines;
    private GameObject[] Cs, Ns, Os, Ss;
    private GameObject C, N, O, S;
    private List<MeshFilter> MFC, MFN, MFO, MFS;

	void Start ()
    {
        f = Resources.Load(file) as TextAsset;
        lines = f.text.Split('\n');
        string[] tmp;
        int last_index = 0;

        //surface.transform.localScale = Vector3.one * scale;

        Hashtable h = new Hashtable();
        h.Add("C", 0);
        h.Add("O", 1);
        h.Add("N", 2);
        h.Add("S", 3);

        SetupStructures(h);

        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].StartsWith("ATOM"))
            {
                tmp = System.Text.RegularExpressions.Regex.Replace(lines[i].Trim(), @"\s+", " ").Split(' ');
                GameObject atom = (GameObject)Instantiate(prefab,
                    new Vector3((float)System.Convert.ToDouble(tmp[6]) * scale,
                    (float)System.Convert.ToDouble(tmp[8]) * scale*-1f,
                    (float)System.Convert.ToDouble(tmp[7]) * scale*-1f),
                    Quaternion.identity);

                atom.transform.localScale = Vector3.one * scale;
                atom.transform.parent = atomParent;

                switch (tmp[10])
                {
                    case "C":
                        MFC.Add(atom.GetComponent<MeshFilter>());
                        break;

                    case "N":
                        MFN.Add(atom.GetComponent<MeshFilter>());
                        break;

                    case "O":
                        MFO.Add(atom.GetComponent<MeshFilter>());
                        break;

                    case "S":
                        MFS.Add(atom.GetComponent<MeshFilter>());
                        break;

                    default:
                        break;
                }

                if(groupBy == Groups.CONS)
                    atom.GetComponent<Renderer>().material = mats[(int)h[tmp[10]]];
                else if (groupBy == Groups.Fifth)
                    atom.GetComponent<Renderer>().material.color = FifthToColor(System.Convert.ToInt16(tmp[5]));
            }
        }

        CombineInstance[] combine;
        
        for (int j = 0; j < sets; j++)
        {
            combine = new CombineInstance[Mathf.Min(chunkSize,(MFC.Count-last_index))];
            for (int i = last_index; i < Mathf.Min(last_index+chunkSize, MFC.Count); i++)
            {
                combine[i-last_index].mesh = MFC[i].sharedMesh;
                combine[i-last_index].transform = MFC[i].transform.localToWorldMatrix;
            }
            Cs[j].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
            Cs[j].transform.parent = dataParent;
            Cs[j].transform.localScale = Vector3.one;
            if (MFC.Count - last_index < chunkSize)
                break;
            last_index += chunkSize;
        }

        last_index = 0;

        for (int j = 0; j < sets; j++)
        {
            combine = new CombineInstance[Mathf.Min(chunkSize, (MFN.Count - last_index))];
            for (int i = last_index; i < Mathf.Min(last_index + chunkSize, MFN.Count); i++)
            {
                combine[i-last_index].mesh = MFN[i].sharedMesh;
                combine[i - last_index].transform = MFN[i].transform.localToWorldMatrix;
            }
            Ns[j].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
            Ns[j].transform.parent = dataParent;
            Ns[j].transform.localScale = Vector3.one;
            if (MFN.Count - last_index < chunkSize)
                break;
            last_index += chunkSize;
        }

        last_index = 0;

        for (int j = 0; j < sets; j++)
        {
            combine = new CombineInstance[Mathf.Min(chunkSize, (MFO.Count - last_index))];
            for (int i = last_index; i < Mathf.Min(last_index + chunkSize, MFO.Count); i++)
            {
                combine[i-last_index].mesh = MFO[i].sharedMesh;
                combine[i - last_index].transform = MFO[i].transform.localToWorldMatrix;
            }
            Os[j].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
            Os[j].transform.parent = dataParent;
            Os[j].transform.localScale = Vector3.one;
            if (MFO.Count - last_index < chunkSize)
                break;
            last_index += chunkSize;
        }

        last_index = 0;

        for (int j = 0; j < sets; j++)
        {
            combine = new CombineInstance[Mathf.Min(chunkSize, (MFS.Count - last_index))];
            for (int i = last_index; i < Mathf.Min(last_index + chunkSize, MFS.Count); i++)
            {
                combine[i-last_index].mesh = MFS[i].sharedMesh;
                combine[i - last_index].transform = MFS[i].transform.localToWorldMatrix;
            }
            Ss[j].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
            Ss[j].transform.parent = dataParent;
            Ss[j].transform.localScale = Vector3.one;
            if (MFS.Count - last_index < chunkSize)
                break;
            last_index += chunkSize;
        }

        Destroy(atomParent.gameObject);
        System.GC.Collect();
        dataParent.transform.position = Vector3.up;
    }

    private Color FifthToColor(int c)
    {
        Color col = Color.white;
        if (c < 100)
            col.r = .8f;
        else
            col.g = .8f;
        //return col;
        return Color.black;
    }

    private void SetupStructures(Hashtable h)
    {
        Cs = new GameObject[sets];
        Ns = new GameObject[sets];
        Os = new GameObject[sets];
        Ss = new GameObject[sets];

        MFC = new List<MeshFilter>();
        MFN = new List<MeshFilter>();
        MFO = new List<MeshFilter>();
        MFS = new List<MeshFilter>();

        /*
        C = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);
        N = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);
        O = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);
        S = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);

        C.name = "CMaster";
        N.name = "NMaster";
        O.name = "OMaster";
        S.name = "SMaster";

        C.GetComponent<MeshFilter>().mesh = new Mesh();
        N.GetComponent<MeshFilter>().mesh = new Mesh();
        O.GetComponent<MeshFilter>().mesh = new Mesh();
        S.GetComponent<MeshFilter>().mesh = new Mesh();

        C.GetComponent<Renderer>().material = mats[(int)h["C"]];
        N.GetComponent<Renderer>().material = mats[(int)h["N"]];
        O.GetComponent<Renderer>().material = mats[(int)h["O"]];
        S.GetComponent<Renderer>().material = mats[(int)h["S"]];
        */

        
        for (int i = 0; i < 50; i++)
        {
            Cs[i] = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);
            Ns[i] = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);
            Os[i] = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);
            Ss[i] = (GameObject)Instantiate(meshPrefab, Vector3.zero, Quaternion.identity);

            Cs[i].name = "C" + i.ToString();
            Ns[i].name = "N" + i.ToString();
            Os[i].name = "O" + i.ToString();
            Ss[i].name = "S" + i.ToString();

            Cs[i].GetComponent<MeshFilter>().mesh = new Mesh();
            Ns[i].GetComponent<MeshFilter>().mesh = new Mesh();
            Os[i].GetComponent<MeshFilter>().mesh = new Mesh();
            Ss[i].GetComponent<MeshFilter>().mesh = new Mesh();

            Cs[i].GetComponent<Renderer>().material = mats[(int)h["C"]];
            Ns[i].GetComponent<Renderer>().material = mats[(int)h["N"]];
            Os[i].GetComponent<Renderer>().material = mats[(int)h["O"]];
            Ss[i].GetComponent<Renderer>().material = mats[(int)h["S"]];
        }
        
    }
}
