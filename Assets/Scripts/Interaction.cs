﻿using UnityEngine;

public class Interaction : MonoBehaviour
{
    public GameObject protein;

    private SteamVR_TrackedController _controller;
    private PrimitiveType _currentPrimitiveType = PrimitiveType.Sphere;

    private void OnEnable()
    {
        _controller = GetComponent<SteamVR_TrackedController>();
        _controller.TriggerClicked += HandleTriggerClicked;
        _controller.PadClicked += HandlePadClicked;
    }

    private void OnDisable()
    {
        _controller.TriggerClicked -= HandleTriggerClicked;
    }

    private void HandleTriggerClicked(object sender, ClickedEventArgs e)
    {
        if( Vector3.Distance(_controller.transform.position, protein.transform.position) < 1f)
            protein.transform.position = _controller.transform.position;
    }

    private void HandlePadClicked(object sender, ClickedEventArgs e)
    {
        if (e.padY < 0)
            protein.transform.Rotate(Vector3.right, .5f);
        else
            protein.transform.Rotate(Vector3.right, -.5f);

        if (e.padX < 0)
            protein.transform.Rotate(Vector3.forward, .5f);
        else
            protein.transform.Rotate(Vector3.forward, -.5f);
    }

    
}