﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ChangeSurface : MonoBehaviour {

    public Material solid;
    public Material transparent;

    private bool isSolid;

    public void SetSolid()
    {
        GetComponent<Renderer>().material = solid;
    }

    public void SetTransparent()
    {
        GetComponent<Renderer>().material = transparent;
    }

    public void Change()
    {
        if (isSolid)
            SetTransparent();
        else
            SetSolid();

        isSolid = !isSolid;
    }
}
