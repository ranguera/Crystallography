﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class SteamVR_TestThrow : MonoBehaviour
{
    public GameObject go;
    public Transform otherController;
    public GameObject surface;

    private Vector3 initialController, initialData;
    private Quaternion prevRot;
    private SteamVR_Controller.Device device;

    private float contrDistance;
    private int surfaceMode;

	SteamVR_TrackedObject trackedObj;

	void Awake()
	{
		trackedObj = GetComponent<SteamVR_TrackedObject>();
        print(trackedObj.index);
    }

    void Start()
    {
        device = SteamVR_Controller.Input((int)trackedObj.index);
    }

	void Update()
	{
        if(device==null)
        {
            device = SteamVR_Controller.Input((int)trackedObj.index);
        }

        if( device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            initialController = this.transform.position;
            initialData = go.transform.position;
            if (prevRot == null)
                prevRot = this.transform.rotation;
        }

		if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
		{
            print(this.trackedObj.index);
            go.transform.position = initialData + (this.transform.position-initialController);
            
            Quaternion rot = Quaternion.Inverse(this.transform.rotation) * prevRot;
            //go.transform.rotation *= rot;
            prevRot = this.transform.rotation;
            //go.transform.rotation *= Quaternion.Inverse(this.transform.rotation) * rData;
        }

        if( device.GetTouchDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            if (surfaceMode == 0 || surfaceMode == 1)
            {
                this.surface.SetActive(true);
                ChangeSurface[] scripts = go.GetComponentsInChildren<ChangeSurface>();
                for (int i = 0; i < scripts.Length; i++)
                {
                    scripts[i].Change();
                }
                surfaceMode++;
            }
            else if(surfaceMode == 2)
            {
                surfaceMode = 0;
                this.surface.SetActive(false);
            }
        }

        if(device.GetTouchDown(SteamVR_Controller.ButtonMask.Grip))
        {
            contrDistance = Vector3.Distance(this.transform.position, otherController.position);
        }

        if (device.GetTouch(SteamVR_Controller.ButtonMask.Grip))
        {
            go.transform.localScale += Vector3.one*.1f * (Vector3.Distance(this.transform.position, otherController.position) - contrDistance);
            contrDistance = Vector3.Distance(this.transform.position, otherController.position);
        }
    }
}
